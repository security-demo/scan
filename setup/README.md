# scan

Scan for push artifacts into cards

## Setup

Configure CI/CD Variables:

* GIT_RW_TOKEN - Masked. Access Token for push commits in to repository cards
* GIT_COMMITTER_NAME - Name of the bot like "bot"
* GIT_COMMITTER_EMAIL - Email of the bot like "bot@example.com"
* CARDS_GIT_URL - URI without proto ie gitlab.com/security-demo/cards.git

Configure CARDS_GIT Vairable in 